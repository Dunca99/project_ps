package com.example.demo;

import com.example.demo.DAO.AdminDAO;
import com.example.demo.DAO.HikesDAO;
import com.example.demo.facade.AdminFacade;
import com.example.demo.facade.HikesFacade;
import com.example.demo.modelLayer.Admin;
import com.example.demo.modelLayer.Hike;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HikeTest {
    @Mock
    HikesDAO hikeDAO;

    @Test
    public void testGetUser() {
        HikesFacade facade = new HikesFacade(hikeDAO);
        facade.getAllHikes();
        verify(hikeDAO, atLeastOnce()).findAll();
    }

    @Test
    public void testAddUser() {
        Hike hike = new Hike("Peleaga", 21, 2);
        HikesFacade facade = new HikesFacade(hikeDAO);
        facade.addUser(hike);
        verify(hikeDAO, atLeastOnce()).save(hike);
    }

    @Test
    public void updateUser() {
        Hike hike = new Hike("Peleaga", 21, 2);
        HikesFacade facade = new HikesFacade(hikeDAO);
        facade.updateUser(hike, Long.valueOf(3));
        verify(hikeDAO, atLeastOnce()).findById(3L);
    }

    @Test
    public void deleteAdmin() {
        HikesFacade facade = new HikesFacade(hikeDAO);
        facade.deleteHike(3L);
        verify(hikeDAO, atLeastOnce()).deleteById(Long.valueOf(3));
    }

    @Mock
    Hike hike2;
    @Test
    public void testGetPrice() {
        HikesFacade facade = new HikesFacade(hikeDAO);
        facade.getPrice(hike2,  Long.valueOf(20));
        verify(hike2, atLeastOnce()).getPrice();
    }
}
