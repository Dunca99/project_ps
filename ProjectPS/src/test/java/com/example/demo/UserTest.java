package com.example.demo;

import com.example.demo.DAO.UserDAO;
import com.example.demo.facade.UserFacade;
import com.example.demo.modelLayer.User;
import com.example.demo.serviceLayer.AdminService;
import com.example.demo.serviceLayer.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserTest {
    @Mock
    UserDAO userDAO;

    @Mock
    AdminService adminService;

    @Test
    public void testGetUser() {
        UserFacade facade = new UserFacade(userDAO);
        facade.getAllUsers();
        verify(userDAO, atLeastOnce()).findAll();
    }

    @Test
    public void testAddUser() {
        User user = new User("Dunca Lucian", 21, "lucian_dunca@yahoo.com", "televizor");
        UserFacade facade = new UserFacade(userDAO);
        facade.addUser(user);
        //DAO mock initializat in service
        verify(userDAO, atLeastOnce()).save(user);
    }

    @Test
    public void updateUser() {
        User user = new User("Dunca Lucian", 21, "lucian_dunca@yahoo.com", "televizor");
        UserFacade facade = new UserFacade(userDAO);
        facade.updateUser(user,  Long.valueOf(3));
        verify(userDAO, atLeastOnce()).findById(3L);
    }

    @Test
    public void deleteAdmin() {
        UserFacade facade = new UserFacade(userDAO);
        facade.deleteUser(3L);
        verify(userDAO, atLeastOnce()).deleteById(Long.valueOf(3));
    }

    @Test
    public void observerTest() {
        UserFacade facadeUser = new UserFacade(userDAO);
        UserService userService = new UserService(adminService, facadeUser);
        userService.notifyObs();
        verify(adminService, atLeastOnce()).update();
    }

}
