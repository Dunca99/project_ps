package com.example.demo;

import com.example.demo.DAO.AdminDAO;
import com.example.demo.facade.AdminFacade;
import com.example.demo.modelLayer.Admin;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdminTest {
    @Mock
    AdminDAO adminDAO;

    @Test
    public void testGetUser() {
        AdminFacade facade = new AdminFacade(adminDAO);
        facade.getAllAdmins();
        verify(adminDAO, atLeastOnce()).findAll();
    }

    @Test
    public void testAddUser() {
        Admin admin = new Admin("Dunca Lucian", 21, "lucian_dunca@yahoo.com", "televizor");
        AdminFacade facade = new AdminFacade(adminDAO);
        facade.addAdmin(admin);
        verify(adminDAO, atLeastOnce()).save(admin);
    }

    @Test
    public void updateUser() {
        Admin admin = new Admin("Dunca Lucian", 21, "lucian_dunca@yahoo.com", "televizor");
        AdminFacade facade = new AdminFacade(adminDAO);
        facade.updateAdmin(admin,  Long.valueOf(3));
        verify(adminDAO, atLeastOnce()).findById(3L);
    }

    @Test
    public void deleteAdmin() {
        AdminFacade facade = new AdminFacade(adminDAO);
        facade.deleteAdmin(3L);
        verify(adminDAO, atLeastOnce()).deleteById(Long.valueOf(3));
    }
}
