package com.example.demo.facade;

import com.example.demo.DAO.AdminDAO;
import com.example.demo.modelLayer.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminFacade {

    @Autowired
    private AdminDAO adminDAO;

    public AdminFacade(AdminDAO adminDAO){ this.adminDAO = adminDAO;}

    public AdminFacade() {

    }

     /**
     * Returns all the users stored in the database
     * @return Returns a list of admins: List<Adm>
     */
    public List<Admin> getAllAdmins(){
        return (List<Admin>) adminDAO.findAll();
    }

    public Admin getAdminById(Long id){
        return adminDAO.findById(id).get();
    }

    /**
     * Adds an admin to the database.
     * @param admin The new admin that will be added to the database.
     */
    public void addAdmin(Admin admin){
        //   facade = new DatabaseFacade<Admin>(adminDAO);
        List<Admin> admins = (List<Admin>) adminDAO.findAll();
        try {
            System.out.println(admins.size());
            if (admins.size() <= 2)
                adminDAO.save(admin);
            else throw new Exception("Max number of admins");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Updates a admin in the database
     * @param newAdmin The new data that will be updated in the database.
     * @param id The id of the admin we want to update.
     */
    public void updateAdmin(Admin newAdmin, Long id){
        adminDAO.findById(id).map( user1 -> {
            user1.setAge(newAdmin.getAge());
            user1.setEmail(newAdmin.getEmail());
            user1.setName(newAdmin.getName());
            user1.setPassword(newAdmin.getPassword());
            return adminDAO.save(user1);
        });
    }

    /**
     * Deletes a admin from the database.
     * @param id  The id of the admin we want to delete.
     */
    public void deleteAdmin( Long id){
        adminDAO.deleteById(id);
    }

    /**
     * Logic to be executed when the class is notified by the observed object.
     */
    public void updateObserver(){
        System.out.println("Obs merge");
    }
}
