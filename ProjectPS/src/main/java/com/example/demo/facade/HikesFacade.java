package com.example.demo.facade;

import com.example.demo.DAO.HikesDAO;
import com.example.demo.DAO.UserDAO;
import com.example.demo.modelLayer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HikesFacade {

    @Autowired
    private HikesDAO hikeDAO;

    public HikesFacade(HikesDAO hikeDAO){
        this.hikeDAO = hikeDAO;
    }

    /**
     * Returns all the hikes stored in the database
     * @return Returns a list of users: List<Hike>
     */
    public List<Hike> getAllHikes(){
        return (List<Hike>) hikeDAO.findAll();
    }

    /**
     * Returns a hike with the matched id.
     * @param id The id of the hike we need to return.
     * @return Returns a hike with the matched id.
     */
    public Hike getUserById(Long id){
        return hikeDAO.findById(id).get();
    }


    /**
     * Adds an hike to the database.
     * @param hike The new hike that will be added to the database.
     */
    public void addUser(Hike hike){
        hikeDAO.save(hike);
    }

    /**
     * Updates a hike in the database
     * @param newHike The new data that will be updated in the database.
     * @param id The id of the hike we want to update.
     */
    public void updateUser(Hike newHike, Long id){
        hikeDAO.findById(id).map( user1 -> {
            user1.setCategory(newHike.getCategory());
            user1.setDestination(newHike.getDestination());
            user1.setDurationDays(newHike.getDurationDays());
            user1.setPrice(newHike.getPrice());
            return hikeDAO.save(user1);
        });
    }

    /**
     * Deletes a hike from the database.
     * @param id  The id of the hike we want to delete.
     */
    public void deleteHike( Long id){
        hikeDAO.deleteById(id);
    }

    /**
     * Compute and reuturn the price of a hike. The price depends on the user age
     * @param hike The hike we want to compute the price for
     * @param age The age of the clinent that requests the price
     * @return An int that represents the price.
     */
    public int getPrice(Hike hike, Long age){
        if(age >= 18){
            PriceStrategy strategy = new AdultPrice();
            hike.setPriceStrategy(strategy);
            return hike.getPrice();
        }else{
            PriceStrategy strategy = new UnderagePrice();
            hike.setPriceStrategy(strategy);
            return hike.getPrice();
        }

    }
}
