package com.example.demo.facade;

import com.example.demo.DAO.UserDAO;
import com.example.demo.modelLayer.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Facade used as a wrapper for service. We implement here all the GET, POST, PUT/DELETE calls.
 */
@Service
public class UserFacade {

    @Autowired
    private UserDAO userDAO;

    public UserFacade(UserDAO userDao){
        this.userDAO = userDao;
    }

    /**
     * Returns all the users stored in the database
     * @return Returns a list of users: List<User>
     */
    public List<User> getAllUsers(){
        return (List<User>) userDAO.findAll();
    }

    /**
     * Returns a user with the matched id.
     * @param id The id of the user we need to return.
     * @return Returns a user with the matched id.
     */
    public User getUserById(Long id){
        return userDAO.findById(id).get();
    }


    /**
     * Adds an user to the database.
     * @param user The new user that will be added to the database.
     */
    public void addUser(User user){
        userDAO.save(user);
    }

    /**
     * Updates a user in the database
     * @param newUser The new data that will be updated in the database.
     * @param id The id of the user we want to update.
     */
    public void updateUser(User newUser, Long id){
        userDAO.findById(id).map( user1 -> {
            user1.setAge(newUser.getAge());
            user1.setEmail(newUser.getEmail());
            user1.setName(newUser.getName());
            user1.setPassword(newUser.getPassword());
            return userDAO.save(user1);
        });
    }

    /**
     * Deletes a user from the database.
     * @param id  The id of the user we want to delete.
     */
    public void deleteUser( Long id){
        userDAO.deleteById(id);
    }

    /**
     * Checks if the user that log in exists in the database and if the password used is corect
     * @param user An user that contains the email and the password of from the log in fields
     * @return Return true if the user is found, else false.
     */
    public boolean verifyUser(User user){
        User foundUser = userDAO.getUserByEmail(user.getEmail());
        if(foundUser == null)
            return false;

        if(foundUser.getPassword().equals(user.getPassword()))
            return true;

        return false;
    }
}
