package com.example.demo.controller;

import com.example.demo.modelLayer.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.serviceLayer.AdminService;

import java.util.List;
/**
 * Controller used for admin actions. We implement here all the GET, POST, PUT/DELETE calls.
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    /**
     * Returns all the users stored in the database
     * @return Returns a list of admins: List<Adm>
     */
    @GetMapping(path="/getAdmin")
    public List<Admin> getAdmin(){
        return adminService.getAllAdmins();
    }


    /**
     * Adds an admin to the database.
     * @param admin The new admin that will be added to the database.
     */
    @PostMapping(path="/addAdmin", consumes="application/json",produces ="application/json")
    public void addAdmin(@RequestBody Admin admin){

        adminService.addAdmin(admin);
    }

    /**
     * Updates a admin in the database
     * @param newAdmin The new data that will be updated in the database.
     * @param id The id of the admin we want to update.
     */
    @PutMapping(path="updateAdmin/{id}")
    public void updateAdmin(@RequestBody Admin newAdmin, @PathVariable Long id){
        adminService.updateAdmin(newAdmin, id);
    }

    /**
     * Deletes a admin from the database.
     * @param id  The id of the admin we want to delete.
     */
    @DeleteMapping(path="deleteAdmin/{id}")
    public void deleteAdmin( @PathVariable Long id){
        adminService.deleteAdmin(id);
    }
}
