package com.example.demo.controller;

import com.example.demo.modelLayer.Hike;
import com.example.demo.modelLayer.User;
import com.example.demo.serviceLayer.HikesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.serviceLayer.UserService;

import java.util.List;
/**
 * Controller used for user actions. We implement here all the GET, POST, PUT/DELETE calls.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private HikesService hikesService;

    /**
     * Returns all the users stored in the database
     * @return Returns a list of users: List<User>
     */
    @GetMapping(path="/getUser")
    public List<User> getAllUsers(){

        return userService.getAllUsers();
    }

    /**
     * Returns a user with the matched id.
     * @param id The id of the user we need to return.
     * @return Returns a user with the matched id.
     */
    @GetMapping(path="/getUser/{id}")
    public User getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    /**
     * Adds an user to the database.
     * @param user The new user that will be added to the database.
     */
    @PostMapping(path="/addUser", consumes="application/json",produces ="application/json")
    public void addUser(@RequestBody User user){
        userService.addUser(user);
        userService.notifyObs();
    }

    /**
     * Updates a user in the database
     * @param newUser The new data that will be updated in the database.
     * @param id The id of the user we want to update.
     */
    @PutMapping(path="updateUser/{id}")
    public void updateUser(@RequestBody User newUser, @PathVariable Long id){
        userService.updateUser(newUser, id);
    }

    /**
     * Deletes a user from the database.
     * @param id  The id of the user we want to delete.
     */
    @DeleteMapping(path="deleteUser/{id}")
    public void deleteUser( @PathVariable Long id){
        userService.deleteUser(id);
    }

    @GetMapping(path="/selectHike/{idHike}")
    public void selectHike(@PathVariable Long idHike){
        Hike hike = hikesService.getUserById(idHike);
        System.out.println(hike.getDestination());
    }
}
