package com.example.demo.controller;

import com.example.demo.modelLayer.Hike;
import com.example.demo.serviceLayer.HikesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Controller used for hike actions. We implement here all the GET, POST, PUT/DELETE calls.
 */
@RestController
@RequestMapping("/hikes")
public class HikesController {

    @Autowired
    private HikesService hikesService;

    /**
     * Returns all the hikes stored in the database
     * @return Returns a list of users: List<Hike>
     */
    @GetMapping(path="/getHikes")
    public List<Hike> getAllUsers(){

        return hikesService.getAllHikes();
    }

    /**
     * Returns a hike with the matched id.
     * @param id The id of the hike we need to return.
     * @return Returns a hike with the matched id.
     */
    @GetMapping(path="/getHike/{id}")
    public Hike getUserById(@PathVariable Long id){
        return hikesService.getUserById(id);
    }

    /**
     * Adds an hike to the database.
     * @param hike The new hike that will be added to the database.
     */
    @PostMapping(path="/addHike", consumes="application/json",produces ="application/json")
    public void addUser(@RequestBody Hike hike){
        hikesService.addHike(hike);
    }

    /**
     * Updates a hike in the database
     * @param newHike The new data that will be updated in the database.
     * @param id The id of the hike we want to update.
     */
    @PutMapping(path="updateHike/{id}")
    public void updateUser(@RequestBody Hike newHike, @PathVariable Long id){
        hikesService.updateHike(newHike, id);
    }

    /**
     * Deletes a hike from the database.
     * @param id  The id of the hike we want to delete.
     */
    @DeleteMapping(path="deleteHike/{id}")
    public void deleteUser( @PathVariable Long id){
        hikesService.deleteHike(id);
    }

    /**
     * Return the price of a hike
     * @param idHike The id of the hike we want to compute the price
     * @param age The age of the client.
     */
    @GetMapping(path="/getPrice/{idHike}/{age}")
    public void getPrice(@PathVariable Long idHike, @PathVariable Long age){
        hikesService.getPrice(hikesService.getUserById(idHike), age);
    }
}
