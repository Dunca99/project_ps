package com.example.demo.controller;

import com.example.demo.modelLayer.User;
import com.example.demo.serviceLayer.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to implement the log in logic.
 */
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private UserService userService;

    @PostMapping(path="/verify", consumes="application/json",produces ="application/json")
    public boolean checkLogIn(@RequestBody User user){
        System.out.println("luci");
        return userService.verifyUser(user);
    }

}
