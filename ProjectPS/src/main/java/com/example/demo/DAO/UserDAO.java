package com.example.demo.DAO;

import com.example.demo.modelLayer.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Interface used to access the database table for users.
 */
@Repository
public interface UserDAO extends CrudRepository<User, Long> {

    @Query("SELECT user FROM User user WHERE user.email = ?1")
    public User getUserByEmail(String email);


}
