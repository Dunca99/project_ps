package com.example.demo.DAO;

import com.example.demo.modelLayer.Admin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Interface used to access the database table for admins.
 */
@Repository
public interface AdminDAO extends CrudRepository<Admin, Long> {

}