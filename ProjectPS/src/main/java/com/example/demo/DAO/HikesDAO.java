package com.example.demo.DAO;

import com.example.demo.modelLayer.Admin;
import com.example.demo.modelLayer.Hike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface used to access the database table for admins.
 */
@Repository
public interface HikesDAO extends CrudRepository<Hike, Long> {


}