package com.example.demo.modelLayer;

public interface PriceStrategy {
    int calculatePrice(int duration, int category);
}
