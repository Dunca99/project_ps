package com.example.demo.modelLayer;

import javax.persistence.*;

@Entity
@Table(name="hikes")
public class Hike {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true, length = 45)
    private String destination;
    @Column
    private int durationDays;
    @Column
    private int category;
    @Column
    private int price;

    @Transient
    private PriceStrategy priceStrategy;

    public Hike(String destination, int durationDays, int category) {
        this.destination = destination;
        this.durationDays = durationDays;
        this.category = category;

    }
    public Hike(String destination, int durationDays, int category, PriceStrategy priceStrategy) {
        this(destination, durationDays, category);
        this.priceStrategy = priceStrategy;
        this.price = priceStrategy.calculatePrice(durationDays, category);
    }

    public Hike() {

    }

    public PriceStrategy getPriceStrategy() {
        return priceStrategy;
    }

    public void setPriceStrategy(PriceStrategy priceStrategy) {
        this.priceStrategy = priceStrategy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(int durationDays) {
        this.durationDays = durationDays;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getPrice() {
        if (getPriceStrategy() != null)
            return priceStrategy.calculatePrice(durationDays, category);
        return 0;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
