package com.example.demo.modelLayer;

public class AdultPrice implements PriceStrategy{
    @Override
    public int calculatePrice(int duration, int category) {
        return  duration * 150  * category;
    }
}
