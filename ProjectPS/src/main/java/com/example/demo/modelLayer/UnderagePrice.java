package com.example.demo.modelLayer;

public class UnderagePrice implements PriceStrategy{
    @Override
    public int calculatePrice(int duration, int category) {
        return duration * 100  * category;
    }
}
