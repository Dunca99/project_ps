package com.example.demo.serviceLayer;

import com.example.demo.facade.AdminFacade;
import com.example.demo.modelLayer.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdminService implements Observer {

    @Autowired
    AdminFacade facade;

    public AdminService(AdminFacade facade){ this.facade = facade;}

    /**
     * Returns all the users stored in the database
     * @return Returns a list of admins: List<Adm>
     */
    public List<Admin> getAllAdmins(){
        return (List<Admin>) facade.getAllAdmins();
    }

    public Admin getAdminByid(Long id){
        return facade.getAdminById(id);
    }


    /**
     * Adds an admin to the database.
     * @param admin The new admin that will be added to the database.
     */
    public void addAdmin(Admin admin){
        facade.addAdmin(admin);
    }

    /**
     * Updates a admin in the database
     * @param newAdmin The new data that will be updated in the database.
     * @param id The id of the admin we want to update.
     */
    public void updateAdmin(Admin newAdmin, Long id){
      facade.updateAdmin(newAdmin, id);
    }

    /**
     * Deletes a admin from the database.
     * @param id  The id of the admin we want to delete.
     */
    public void deleteAdmin( Long id){
        facade.deleteAdmin(id);
    }


    /**
     * Logic to be executed when the class is notified by the observed object.
     */
    @Override
    public void update() {
        facade.updateObserver();
    }


}
