package com.example.demo.serviceLayer;

/**
 * Observer interface to be implemented by the classes that needs to have execute something when the
 * observed class changes.
 */
public interface Observer {

    public void update();
}
