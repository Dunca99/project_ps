package com.example.demo.serviceLayer;

import com.example.demo.facade.HikesFacade;
import com.example.demo.modelLayer.Hike;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HikesService {

    @Autowired
    HikesFacade facade;

    /**
     * Returns all the hikes stored in the database
     * @return Returns a list of users: List<Hike>
     */
    public List<Hike> getAllHikes(){
        return (List<Hike>) facade.getAllHikes();
    }

    /**
     * Returns a hike with the matched id.
     * @param id The id of the hike we need to return.
     * @return Returns a hike with the matched id.
     */
    public Hike getUserById(Long id){
        return facade.getUserById(id);
    }

    /**
     * Adds an hike to the database.
     * @param hike The new hike that will be added to the database.
     */
    public void addHike(Hike hike){
        facade.addUser(hike);
    }

    /**
     * Updates a hike in the database
     * @param newHike The new data that will be updated in the database.
     * @param id The id of the hike we want to update.
     */
    public void updateHike(Hike newHike, Long id){
        facade.updateUser(newHike, id);
    }

    /**
     * Deletes a hike from the database.
     * @param id  The id of the hike we want to delete.
     */
    public void deleteHike( Long id){
        facade.deleteHike(id);
    }

    /**
     * Compute and reuturn the price of a hike. The price depends on the user age
     * @param hike The hike we want to compute the price for
     * @param age The age of the clinent that requests the price
     * @return An int that represents the price.
     */
    public int getPrice(Hike hike, Long age){
        return facade.getPrice(hike, age);
    }
}
