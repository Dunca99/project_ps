package com.example.demo.serviceLayer;

/**
 * Observable abstract class to be inherited by the classes that needs to notify the classes that implements
 * the Observer interface.
 */
public abstract class Observable {

    private Observer observer;

    /**
     * Initializes the observer field
     * @param ob
     */
    public void initObserver(Observer ob) {
        observer = ob;
    }


    /**
     * Notifies the observer that a change occurred.
     */
    public void notifyObs() {
        observer.update();
    }


}
