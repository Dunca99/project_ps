package com.example.demo.serviceLayer;

import com.example.demo.facade.UserFacade;
import com.example.demo.modelLayer.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Service used to implement the action's logic. We implement here all the GET, POST, PUT/DELETE calls.
 */
@Service
public class UserService extends Observable {

    @Autowired
    private UserFacade facade;

    @Autowired
    private AdminService adminService;

    @PostConstruct
    public void init() {
        initObserver(adminService);
    }

    public UserService(AdminService adminService, UserFacade userFacade){
        this.adminService = adminService;
        initObserver(adminService);
        this.facade = userFacade;
    }

    /**
     * Returns all the users stored in the database
     * @return Returns a list of users: List<User>
     */
    public List<User> getAllUsers(){
        return (List<User>) facade.getAllUsers();
    }

    /**
     * Returns a user with the matched id.
     * @param id The id of the user we need to return.
     * @return Returns a user with the matched id.
     */
    public User getUserById(Long id){
        return facade.getUserById(id);
    }

    /**
     * Adds an user to the database.
     * @param user The new user that will be added to the database.
     */
    public void addUser(User user){
        facade.addUser(user);
    }

    /**
     * Updates a user in the database
     * @param newUser The new data that will be updated in the database.
     * @param id The id of the user we want to update.
     */
    public void updateUser(User newUser, Long id){
        facade.updateUser(newUser, id);
    }

    /**
     * Deletes a user from the database.
     * @param id  The id of the user we want to delete.
     */
    public void deleteUser( Long id){
        facade.deleteUser(id);
    }


    /**
     * Checks if the user that log in exists in the database and if the password used is corect
     * @param user An user that contains the email and the password of from the log in fields
     * @return Return true if the user is found, else false.
     */
    public boolean verifyUser(User user){
        return facade.verifyUser(user);
    }
}
